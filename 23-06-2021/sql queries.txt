               DATABASE
To create a database -
->create database Divergent;

To switch to a databse -
->use Divergent;

To delete a database-
->drop Divergent of database;

To see all the tables in the database-
->show tables;

To show how many databases are there-
->show database;

               TAbLES
To show all data in the table-
->select * from Divergent;

To delete a table -
->drop Divergent of table;

To add column-
->alter table divergent add col(name,varchar);

To insert values-
->insert into divergent values(priyanshi);

To delete a record from table-
->delete from table divergent where col=name;

To update record-
->update table divergent set col priyanshi_sahu=update name where col=priyanshi;

To counting records-
->select count name from table;

To counting and selecting group record-
->select count(*)from divergent group by name;

To removing table column-
->alter table divergent drop col name;

To count a specific entry of a specific column-
->select count(*) from table divergent where name="priyanshi";

To change the column name or type of column-
->alter table divergent change name new NAME type;
  
To sort the data-
->select * from divergent (condition) order by NAME;

To join two tbles data-
->SELECT c.first_name, c.last_name, a.address
-> FROM customer c JOIN address;

